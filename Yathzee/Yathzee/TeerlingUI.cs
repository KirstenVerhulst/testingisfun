﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yathzee
{
  public partial class TeerlingUI : UserControl
  {
    private Teerling model;
    private TeerlingController controller;
    public TeerlingUI(Teerling tModel, TeerlingController tController)
    {
      InitializeComponent();
      model = tModel;
      controller = tController;
      UpdateIU();
    }

    private void btnNumber_Click(object sender, EventArgs e)
    {
      controller.lblAAntalogenClicked();
      UpdateIU();
    }

    private void btnWerp_Click(object sender, EventArgs e)
    {
      controller.btnWerpClicked();
      btnNumber.Text = Convert.ToString(model.GetAantalOgen());
      UpdateIU();
    }

    public void UpdateIU()
    {
      btnNumber.BackColor = model.IsVast() ? Color.Red : Color.Green;
      btnNumber.Text = Convert.ToString(model.GetAantalOgen());
    }
  }
}//je kan ook alles in een klasse updateUI zetten en deze telkens oproepen om interface te updaten
