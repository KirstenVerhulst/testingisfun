﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yathzee
{
  public class Teerling
  {

    public Teerling() 
    {
      if (generator == null)
      {
         generator= new Random();
      }
      aantalOgen = generator.Next(1, 7);
    }

    protected int aantalOgen;
    protected Boolean vast;
    private List<TeerlingUI> observers = new List<TeerlingUI>();

    protected static Random generator;
    //bwahahahahahahahahahahahah    hah
    public int GetAantalOgen()
    {

      return aantalOgen;
    }

    public Boolean IsVast()
    {
      return vast;
    }

    public void Werp()
    {
      if (!vast)
      {
        aantalOgen = generator.Next(1, 7);
      }
      NotifyObservers();
    }

    public void MaakLos()
    {
      vast = false;
      NotifyObservers();
    }

    public void ZetVast() 
    {
      vast = true;
      NotifyObservers();
    }
    public void AddObserver(TeerlingUI observer)
    {
      observers.Add(observer);
    }

    private void NotifyObservers() 
    {
      foreach (TeerlingUI tUI in observers)
      {
        tUI.UpdateIU();
      }
    }
  }
}
