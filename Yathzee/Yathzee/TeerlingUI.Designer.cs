﻿namespace Yathzee
{
  partial class TeerlingUI
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.btnWerp = new System.Windows.Forms.Button();
      this.btnNumber = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // btnWerp
      // 
      this.btnWerp.Location = new System.Drawing.Point(42, 165);
      this.btnWerp.Name = "btnWerp";
      this.btnWerp.Size = new System.Drawing.Size(165, 46);
      this.btnWerp.TabIndex = 0;
      this.btnWerp.Text = "Werp";
      this.btnWerp.UseVisualStyleBackColor = true;
      this.btnWerp.Click += new System.EventHandler(this.btnWerp_Click);
      // 
      // btnNumber
      // 
      this.btnNumber.BackColor = System.Drawing.Color.Green;
      this.btnNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnNumber.Location = new System.Drawing.Point(42, 3);
      this.btnNumber.Name = "btnNumber";
      this.btnNumber.Size = new System.Drawing.Size(165, 156);
      this.btnNumber.TabIndex = 1;
      this.btnNumber.Text = "1";
      this.btnNumber.UseVisualStyleBackColor = false;
      this.btnNumber.Click += new System.EventHandler(this.btnNumber_Click);
      // 
      // TeerlingUI
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.btnNumber);
      this.Controls.Add(this.btnWerp);
      this.Name = "TeerlingUI";
      this.Size = new System.Drawing.Size(247, 238);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Button btnWerp;
    private System.Windows.Forms.Button btnNumber;
  }
}
