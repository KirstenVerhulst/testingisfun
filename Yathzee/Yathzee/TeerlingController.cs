﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yathzee
{
  public class TeerlingController
  {
    protected Teerling mTeerling;

    public TeerlingController(Teerling model) 
    {
      mTeerling = model;
    }

    public TeerlingUI getTeerlingUI()
    {
      TeerlingUI teerlingUI = new TeerlingUI(mTeerling,this);
      mTeerling.AddObserver(teerlingUI);
      return teerlingUI;
    }


    public void lblAAntalogenClicked()
    {
      if (this.mTeerling.IsVast())
      {
        this.mTeerling.MaakLos();
      }
      else
      {
        this.mTeerling.ZetVast();
      }
    }

    public void btnWerpClicked()
    {
      mTeerling.Werp();
    }
  }
}
